# Pokemon trainer application

This is a simple pokemon trainer application using Angular Framework.
It will store a new user when logged in.

Once logged in you will be navigated to the pokemon page, where you can catch any pokemon you would like from the list of pokemons.

All pokemon who got caught gets added to the API and will be shown on the profile page where you also have the option to release caught pokemons.

You can navgigate between profile, pokemon page and sign out through the navbar when logged in.

## Planning 

In the Repo you will find our project planning and structure over our component tree from figma.

## Install

```
git clone https://gitlab.com/Mohxmedzz/pokemon-trainer-angular-application.git
cd ng-pokemon
npm install
```

## Connect with heroku api

Create new folder named `environments` in src folder, in that folder create two files.
(replace the `#` with your key, and the url location):  
1. environment.prod.ts (when you are deploying your appplication) 
export const environment = {

  production: true,

  apiTrainer: '##########',

  apiPokemons: '##########',

  pokemonImages: '##########',

};
2. environment.ts (when you do local development)
export const environment = {

  production: false,

  apiTrainer: '##########',

  apiPokemons: '##########',

  pokemonImages: '##########',

  apiKey: '##########'

};



## Usage

```
ng serve
```

Run ng serve in the cmd terminal, it will start the application and open the browser at `localhost:4200`.

The page will reload when you make changes.\
You may also see any lint errors in the console.

## Contributors 

[Mohamed Abdel Monem (@Mohxmedzz)](@Mohxmedzz)
[Patrick Hajiyahya (@moshixd)](@moshixd)

## Contributing

No contributions allowed. 

## Licence 

© 2022, Mohamed & Patrick
