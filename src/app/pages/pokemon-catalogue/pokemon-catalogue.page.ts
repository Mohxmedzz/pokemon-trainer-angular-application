import { Component, OnInit } from '@angular/core';
import { PokemonDetails } from 'src/app/models/pokemon.model';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.page.html',
  styleUrls: ['./pokemon-catalogue.page.css']
})
export class PokemonCataloguePage implements OnInit {

  get pokemons(): PokemonDetails[]{
    return this.pokemonCatalogueService.pokemons;
  }

  get loading(): boolean{
    return this.pokemonCatalogueService.loading;
  }

  get error(): string {
    return this.pokemonCatalogueService.error;
  }

  constructor(
    private readonly pokemonCatalogueService: PokemonCatalogueService
  ) { }
  // from service, getting all fetched pokemons, pushing the data further to components, displays pokemon on pokemon page
  ngOnInit(): void {
    this.pokemonCatalogueService.findAllPokemons();
  }

}
