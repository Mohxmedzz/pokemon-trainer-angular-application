import { PokemonDetails } from "./pokemon.model";

// model for user, also for pushing up user to the api for storing
export interface Trainer{
    id: number;
    username: string;
    pokemon: PokemonDetails[];
}