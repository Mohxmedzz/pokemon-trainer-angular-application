
// model for fetching data from api
export interface Pokemon{
    count: string;
    next: string;
    previous?: string;
    results: Result[];
}
// model for fetching data from api
export interface Result{
    name: string;
    url: string;
}

// model for pushing in data from api
export interface PokemonDetails{
    id: number;
    name: string;
    sprites: string;
}