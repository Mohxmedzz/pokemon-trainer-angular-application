import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
// handles the logout function, pops up a window with confirmation to log out or stay
  public logout(): void {
    Swal.fire({
      title: 'Do you want to log out?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Log out, see you next time!',
      cancelButtonText: 'Stay, catch more pokemons!',
    }).then((result)=>{
      if (result.value) {
        sessionStorage.clear();
        window.location.reload();
        Swal.fire('You are now leaving the wild grass!', 'Until next time!')
      }else if(result.dismiss === Swal.DismissReason.cancel){
        Swal.fire('Keep on catching!')
      }
    });
  }
}
