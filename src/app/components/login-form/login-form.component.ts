import { Component, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { switchAll } from 'rxjs';
import { Trainer } from 'src/app/models/trainer.model';
import { LoginService } from 'src/app/services/login.service';
import { TrainerService } from 'src/app/services/trainer.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent {

  @Output() login: EventEmitter<void> = new EventEmitter;
  
  constructor(
    private readonly loginService: LoginService,
    private readonly trainerService: TrainerService,
  ) { }
  
  // Login form for logging in and creating users
  public loginSubmit(loginForm: NgForm): void {

    // username!
    const { username } = loginForm.value;

    // stops from logging in without typing username to either log in or create new user
    if (username.length == 0) {
      Swal.fire("You need type in a username or a current user!")    
    }
    else{
      this.loginService.login(username)
      .subscribe({
        next: (trainer: Trainer) => {
          this.trainerService.trainer = trainer;
          this.login.emit();
        },
        error: () => {
          
        }
      })
    }
  }

}
