import { Component, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  // Navbar component, adds routes/links to different pages when logged in.
  get trainer(): Trainer | undefined { 
    return this.userService.trainer;
  }

  constructor(
    private readonly userService: TrainerService
  ) { }

  ngOnInit(): void {
  }

}
