import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { FavouriteService } from 'src/app/services/favourite.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-favourite-button',
  templateUrl: './favourite-button.component.html',
  styleUrls: ['./favourite-button.component.css']
})
export class FavouriteButtonComponent implements OnInit {

  public loading: boolean = false;
  public isFavourite: boolean = false;

  @Input() pokemonId: number = 0;

  constructor(
    private userService: TrainerService,
    private readonly favouriteService: FavouriteService
  ) { }

  ngOnInit(): void {
    this.isFavourite = this.userService.inFavourites(this.pokemonId);
  }
  
  // Adds function to button, add pokemon to caught/favourites
  onFavouriteClick(): void {
    this.loading = true;
    this.favouriteService.addToFavourites(this.pokemonId)
    .subscribe({
      next: (trainer: Trainer) => {
        this.loading = false;
        this.isFavourite = this.userService.inFavourites(this.pokemonId);
      },
      error: (error: HttpErrorResponse) => {
          console.log("ERROR", error.message);
      },
    })
  }

}
