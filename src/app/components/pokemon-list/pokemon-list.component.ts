import { Component, Input, OnInit } from '@angular/core';
import { PokemonDetails } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent implements OnInit {

  //displays list of pokemons from list item component
  @Input() pokemons: PokemonDetails[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
