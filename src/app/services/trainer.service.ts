import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Pokemon, PokemonDetails } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { StorageUtil } from '../utils/storage.utils';

@Injectable({
  providedIn: 'root'
})
export class TrainerService {

  private _trainer?: Trainer;

  get trainer(): Trainer | undefined {
    return this._trainer;
  }

  // Saves created user to sessionstorage
  set trainer(trainer: Trainer | undefined) {
    StorageUtil.storageSave<Trainer>(StorageKeys.Trainer, trainer!);
    this._trainer = trainer;
  }

  constructor() {
    this._trainer = StorageUtil.storageRead<Trainer>(StorageKeys.Trainer);
  }

  // Checks if pokemon is caught/in favourites
  public inFavourites(pokemonId: number): boolean{
    if(this._trainer){
      return Boolean(this.trainer?.pokemon.find((pokemon: PokemonDetails) => pokemon.id === pokemonId));
    }
    return false;
  }

  // Adds pokemon to caught/favourites
  public addToFavourites(pokemon: PokemonDetails): void{
    if (this._trainer) {
      this._trainer.pokemon.push(pokemon);
    }
  }

  // Removes pokemon from caught/favourites
  public removeFromFavourites(pokemonId: number): void{
    if (this._trainer) {
      this._trainer.pokemon = this._trainer.pokemon.filter((pokemon: PokemonDetails) => pokemon.id !== pokemonId);
    }
  }
}
