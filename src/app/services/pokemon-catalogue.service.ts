import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { StorageKeys } from '../enums/storage-keys.enum';
import { PokemonDetails, Result, Pokemon } from '../models/pokemon.model';
import { StorageUtil } from '../utils/storage.utils';

const { apiPokemons, pokemonImages } = environment;

@Injectable({
  providedIn: 'root'
})
export class PokemonCatalogueService {

  private _pokemons: PokemonDetails[] = [];
  private _error: string = "";
  private _loading: boolean = false;
  
    get pokemons(): PokemonDetails[] {
      return this._pokemons;
    }
  
    get error(): string {
      return this._error;
    }
    
    get loading(): boolean {
      return this._loading;
    }

  constructor(private readonly http: HttpClient) { }
  
  // Fetch pokemons from API, if pokemon is fetched do nothing
  // if not then fetch and maps pokemon, saves to sessionstorage
  public findAllPokemons(): void{

    if (this._pokemons.length > 0 || this.loading) {
      return;
    }

    this._loading = true;
    this.http.get<Pokemon>(apiPokemons)
    .pipe(
      map((pokemon: Pokemon) => {
        let index: number = 0;
        pokemon.results.forEach((results: Result) => {
          this._pokemons[index] = {id: index + 1, name: results.name, sprites: `${pokemonImages}/${index+1}.png` }
          index++
        });
        return this._pokemons
      }),

      finalize(() => {
      this._loading = false;
      })
    )
    .subscribe({
      next: response => {
        this._pokemons = response;
        StorageUtil.storageSave(StorageKeys.Trainer, this._pokemons);
        console.log(this._pokemons);
      },
      error: (error: HttpErrorResponse) => {
        this._error = error.message;
      }
    })
  }
  // Find pokemon by Id
  public pokemonById(id: number): PokemonDetails | undefined {
    return this._pokemons.find((pokemon: PokemonDetails) => pokemon.id === id);
  }
}
