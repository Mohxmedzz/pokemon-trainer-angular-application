import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon, PokemonDetails } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { PokemonCatalogueService } from './pokemon-catalogue.service';
import { TrainerService } from './trainer.service';

const {apiKey, apiTrainer} = environment

@Injectable({
  providedIn: 'root'
})
export class FavouriteService {

  constructor(
    private http: HttpClient,
    private readonly pokemonService: PokemonCatalogueService,
    private readonly trainerService: TrainerService,
  ) { }
  
  // Adds pokemon to caught/favourites function, pushing up to api,
  // patching user with new caught/favourite pokemon
  public addToFavourites(pokemonId: number): Observable<Trainer> {
    if (!this.trainerService.trainer) {
      throw new Error("addToFavourites: There is no trainer");
    }

    const trainer: Trainer = this.trainerService.trainer;
    
    const pokemon: PokemonDetails | undefined = this.pokemonService.pokemonById(pokemonId);

    if (!pokemon) {
      throw new Error("addToFavourites: No pokemon with id: " + pokemonId);
    }

    if (this.trainerService.inFavourites(pokemonId)) {
      this.trainerService.removeFromFavourites(pokemonId); 
    }else{
      this.trainerService.addToFavourites(pokemon);
    }

    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKey
    });

    return this.http.patch<Trainer>(`${apiTrainer}/${trainer.id}`, {
      pokemon: [...trainer.pokemon]
    }, {
      headers
    })
    .pipe(
      tap((updatedTrainer: Trainer) =>{
        this.trainerService.trainer = updatedTrainer;
      })
    )
  }
}
