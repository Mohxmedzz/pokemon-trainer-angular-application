import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Trainer } from '../models/trainer.model';

const { apiTrainer, apiKey } = environment

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  // Dependency Injection.
  /*Private bcz we don't want the client to have direct access to the Http */
  constructor(private readonly http: HttpClient) { }

  // Models, HttpClient, Observables, and RxJS operators.
  public login(username: string): Observable<Trainer>{
    return this.checkUsername(username) 
      .pipe(
        switchMap((user: Trainer | undefined) => {
          if (user === undefined ) {
            return this.createUser(username);
          }
          return of(user)
        })
      )
  }


  // Check if user exists
  private checkUsername(username: string): Observable<Trainer | undefined> {
    return this.http.get<Trainer[]>(`${apiTrainer}?username=${username}`)
    .pipe(
      // RxJS Operators
      map((response: Trainer[]) => response.pop())
    )
  }

  // Create a user, posting new user to the api
  private createUser(username: string): Observable<Trainer> {
    const user= {
      username,
      pokemon: []
    };
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": apiKey
    });

    return this.http.post<Trainer>(apiTrainer, user, {
      headers
    })
  }

}
